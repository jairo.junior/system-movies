# Sistema de Filmes

Este projeto foi gerado com [Angular CLI] (https://github.com/angular/angular-cli) versão 7.0.1.

Implementações:  
- [x] Tela com listagem de filmes;
- [x] Buscar filme por nome (campo obrigatório);
- [x] Buscar filme por ano;
- [x] Ordenar filme por: Ordem alfabética A-Z;
- [x] Ordenar filme por: Ordem alfabética Z-A;
- [x] Ordenar filme por: Ordem Maior nota (baseado em média de ratings);
- [x] Ordenar filme por: Ordem Menor nota (baseado em média de ratings);
- [x] Ordenar filme por: Ordem Menor nota (baseado em média de ratings);
- [x] Calcular média de ratings baseado nos ratings de cada filme;
- [x] Tela com informações detalhadas do filme;
- [ ] Testes unitários;
- [X] **Implemntação de recurso de troca de tema;**

## Intalação de pacotes

Execute `npm install` para instalar pacotes node do projeto.

## Servidor de desenvolvimento

Execute `ng serve` para um servidor de desenvolvimento. Navegue até `http://localhost:4200/`. O aplicativo será recarregado automaticamente se você alterar qualquer um dos arquivos do projeto.

## Comandos para criar itens no Angular

Execute `ng generate component-name` para gerar um novo componente. Você também pode usar `ng generate directive | pipe | service | class | guard | interface | enum | module`.

## Método de Build

Execute `ng build` para construir o projeto. Os artefatos de construção serão armazenados no diretório `/dist`. Use o parâmetro `--prod` para uma versão de produção.

## Executando testes de unidade

Execute `ng test` para executar os testes de unidade via [Karma] (https://karma-runner.github.io).

## Executando testes ponta a ponta

Execute `ng e2e` para executar os testes de ponta a ponta via [Protractor] (http://www.protractortest.org/).

## Ajuda adicional

Para obter mais ajuda sobre o Angular CLI, use `ng help` ou verifique o [Angular CLI README] (https://github.com/angular/angular-cli/blob/master/README.md).