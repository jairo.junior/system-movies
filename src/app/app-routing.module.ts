import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailsComponent } from './pages/details/details.component';
import { HomeComponent } from './pages/home/home.component';

const routes: Routes = [
  // Redireciona para home
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  // Rota da home
  { path: 'home', component: HomeComponent },
  // Rota de detralhes de filme (recebe id do filme)
  { path: 'details/:id', component: DetailsComponent },
  // Roda inválida volta para a home
  { path: '**', redirectTo: 'home' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
