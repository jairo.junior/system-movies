import { Movie } from "./movie.model";

/**
 * Entidade que representa uma busca de filme
 *
 * @export
 * @class SearchMovie
 */
export class SearchMovie {
    public Search: Movie[];

    public Response: string;

    public totalResults: number;
}