/**
 * Entidade que representa um rating
 * @export
 * @class Rating
 */
export class Rating {
    public Source: string;
    
    public Value: string;
}