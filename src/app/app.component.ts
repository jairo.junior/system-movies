import { Component } from '@angular/core';
import { ThemeIdentifier } from './identifiers/theme.identifier';
import { ThemeService } from './services/ui/theme.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  // Armazena o tema selecionado
  public theme: ThemeIdentifier;

  constructor(
    private themeService: ThemeService
  ) {
    if (this.themeService.storage) {
      this.themeService.theme = this.themeService.storage;
    }
    else {
      this.themeService.theme = ThemeIdentifier.light;
    }

    this.theme = this.themeService.theme;
  }

  /**
   * Método que altera do tema ao selecionar opção do layout
   *
   * @param {ThemeIdentifier} event
   * @memberof AppComponent
   */
  onThemeChange(event: ThemeIdentifier) {
    this.themeService.theme = event;
    this.theme = this.themeService.theme;
  }
}
