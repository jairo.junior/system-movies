import { Inject, Injectable } from '@angular/core';
import { ThemeIdentifier } from 'src/app/identifiers/theme.identifier';
import { DOCUMENT } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {

  // Armazena identificadores de tema
  private _theme: ThemeIdentifier;

  // Armazena idenficador do objeto salvo no localStorage
  private themeStorage: string = "system-movie-theme";

  constructor(
    @Inject(DOCUMENT) private document: Document
  ) { }

  /**
   * Metodo que recebe tema selecionado
   *
   * @memberof ThemeService
   */
  set theme(theme: ThemeIdentifier) {
    this._theme = theme;

    this.changeDocumentTheme(theme.toString());
    this.storage = theme.toString();
  }

  /**
   * Método que retorna tema selecionado
   *
   * @memberof ThemeService
   */
  get theme() {
    return this._theme;
  }

  /**
   * Método que retorna tema salvo no localStorage
   *
   * @type {string}
   * @memberof ThemeService
   */
  get storage(): string {
    return localStorage.getItem(this.themeStorage);
  }

  /**
   * Método que salva tema selecionado no localStorage
   *
   * @memberof ThemeService
   */
  set storage(value: string) {
    localStorage.setItem(this.themeStorage, value.toString());
  }

  /**
   * Método que aplica 'class' com estilo do tema seleconado no 'document.body'
   *
   * @param {ThemeIdentifier} theme
   * @memberof ThemeService
   */
  changeDocumentTheme(theme: ThemeIdentifier) {
    document.body.classList.remove(ThemeIdentifier.dark);
    document.body.classList.remove(ThemeIdentifier.light);

    switch (theme) {
      case ThemeIdentifier.light:
        document.body.classList.add(ThemeIdentifier.light);
        break;

      case ThemeIdentifier.dark:
        document.body.classList.add(ThemeIdentifier.dark);
        break;

      default:
        document.body.classList.add(ThemeIdentifier.light);
        break;
    }

  }
}
