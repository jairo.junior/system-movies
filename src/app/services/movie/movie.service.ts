import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Movie } from 'src/app/models/movie.model';
import { SearchMovie } from 'src/app/models/search-movie.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MovieService {
  // Armazena opções de rota para a api
  private route: any;

  // Armazena url base da api
  private urlBase: string;

  constructor(
    private http: HttpClient
  ) {
    this.urlBase = `${environment.apiUrl}`
    this.route = {
      base: this.urlBase
    }
  }

  /**
   * Método de listagem de filmes parametrizadas
   *
   * @returns {Observable<SearchMovie>}
   * @memberof MovieService
   */
  list(params: {
    plot?: string
    s?: string,
    y?: number,
    type?: string,
  }): Observable<SearchMovie> {
    return this.http.get<SearchMovie>(this.route.base, { params: JSON.parse(JSON.stringify(params)) });
  }

  /**
   * Método para buscar video de acordo com id
   *
   * @returns {Observable<Movie>}
   * @memberof MovieService
   */
  getById(params: {
    plot?: string
    i?: string,
  }): Observable<Movie> {
    return this.http.get<Movie>(this.route.base, { params: JSON.parse(JSON.stringify(params)) });
  }
}
