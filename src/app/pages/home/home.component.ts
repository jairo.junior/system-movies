import { Component, OnInit } from '@angular/core';
import { MatSelectChange } from '@angular/material/select';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { FilterOrderType } from 'src/app/enums/filter-order-type.enum';
import { Movie } from 'src/app/models/movie.model';
import { Rating } from 'src/app/models/rating.model';
import { MovieService } from 'src/app/services/movie/movie.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  // Armazena lista de filmes com resultado da requisição da api
  public movieList: Movie[] = [];

  // Armazena objeto de opções de ordenação
  public orderOptions: unknown;

  // Flag que indica carregamento da requisição
  public loading: boolean;

  // Flag que indica se a requisição teve resultado ou não
  public noResults: boolean;

  // Armazena subscription de listagem de filmes
  public subscriptionList: Subscription;

  // Armazena subscription de filmes
  public subscriptionGet: Subscription[] = [];

  constructor(
    private router: Router,
    private activatedRouter: ActivatedRoute,
    private movieService: MovieService,
  ) { }

  /**
  * Méto executado na criação do componente
  */
  ngOnInit(): void {

    this.orderOptions = [
      { value: FilterOrderType.alphabeticalAToZ, text: 'Nome (A - Z)' },
      { value: FilterOrderType.alphabeticalZToA, text: 'Nome (Z - A)' },
      { value: FilterOrderType.higherAverage, text: 'Maior nota' },
      { value: FilterOrderType.lowerAverage, text: 'Menor nota' }
    ];
  }

  /**
  * Méto executado quando o componente é destruido
  */
  ngOnDestroy(): void {
    this.subscriptionList.unsubscribe();
    this.subscriptionGet.forEach(element => element.unsubscribe());    
  }

  /**
   * Método que envia para a roda de detalhes passando o id do vídeo como parâmetro
   *
   * @param {string} id
   * @memberof HomeComponent
   */
  onOpenDetails(id: string) {
    this.router.navigate(['../details/' + id], { relativeTo: this.activatedRouter })
  }

  /**
   * Método para filtrar vídeos de acordo com opções selecinar na componente de filtro
   *
   * @param {*} event
   * @memberof HomeComponent
   */
  onFilter(event: any) {
    this.movieList = [];
    this.loading = true;
    this.noResults = false;

    this.subscriptionList = this.movieService.list({
      plot: 'full',
      s: event.name,
      y: event.year,
      type: 'movie'
    }).subscribe(
      result => {
        this.loading = false;
        this.noResults = result.Response.toLowerCase() === 'false';

        if (this.noResults) return;

        this.movieList = result.Search;

        this.movieList.forEach((element, index) => {
          this.subscriptionGet[index] = this.movieService.getById({ i: element.imdbID }).subscribe(result => {
            element.RatingsMedia = this.mediaCalc(result.Ratings);
          });
        });
      },
      error => {
        console.error(error)
      }
    );
  }

  /**
   * Método para limpar filtros
   *
   * @memberof HomeComponent
   */
  onClearFilter() {
    this.movieList = [];
    this.noResults = true;
  }

  /**
   * Método para calcular média de ratings de um filme
   *
   * @param {Rating[]} ratings
   * @returns {number}
   * @memberof HomeComponent
   */
  mediaCalc(ratings: Rating[]): number {
    let calc: number = 0;
    let media: number = 0;

    ratings.forEach(element => {
      if (element.Value.includes('/')) {
        let a = parseFloat(element.Value.split('/')[0]);
        let b = parseFloat(element.Value.split('/')[1]);
        calc += (a / b) * 100;
      } else {
        calc += parseFloat(element.Value);
      }
      media = ~~(calc / ratings.length)
    });

    return media;
  }

  /**
   * Método que ordena lista de acordo com a opção selecionada
   *
   * @param {MatSelectChange} [event]
   * @memberof HomeComponent
   */
  onOrderChange(event?: MatSelectChange) {
    switch (event.value) {
      case FilterOrderType.alphabeticalAToZ:
        this.movieList = this.orderByTitle(this.movieList, FilterOrderType.alphabeticalAToZ);
        break;
      case FilterOrderType.alphabeticalZToA:
        this.movieList = this.orderByTitle(this.movieList, FilterOrderType.alphabeticalZToA);
        break;

      case FilterOrderType.higherAverage:
        this.movieList = this.orderByMedia(this.movieList, FilterOrderType.higherAverage)
        break;

      case FilterOrderType.lowerAverage:
        this.movieList = this.orderByMedia(this.movieList, FilterOrderType.lowerAverage)
        break;

      default:
        this.movieList = this.orderByTitle(this.movieList, FilterOrderType.alphabeticalAToZ);
        break;
    }
  }

  /**
   * Método que ordena lista por título (A-Z / Z-A)
   *
   * @param {Movie[]} list
   * @param {number} order
   * @returns {Movie[]}
   * @memberof HomeComponent
   */
  orderByTitle(list: Movie[], order: number): Movie[] {
    return list.sort(function (a, b) {
      var titleA = a.Title.toUpperCase();
      var textB = b.Title.toUpperCase();

      if (order == FilterOrderType.alphabeticalAToZ) {
        return (titleA < textB) ? -1 : (titleA > textB) ? 1 : 0;
      }
      if (order == FilterOrderType.alphabeticalZToA) {
        return (titleA > textB) ? -1 : (titleA < textB) ? 1 : 0;
      }
    });
  }

  /**
   * Método que ordena lista por média calculada (Maio nota / Menor nota)
   *
   * @param {Movie[]} list
   * @param {number} order
   * @returns {Movie[]}
   * @memberof HomeComponent
   */
  orderByMedia(list: Movie[], order: number): Movie[] {
    if (order == FilterOrderType.higherAverage) {
      return list.sort((a, b) => b.RatingsMedia - a.RatingsMedia);
    }
    if (order == FilterOrderType.lowerAverage) {
      return list.sort((a, b) => a.RatingsMedia - b.RatingsMedia);
    }
  }
}
