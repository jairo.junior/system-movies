import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Movie } from 'src/app/models/movie.model';
import { MovieService } from 'src/app/services/movie/movie.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  // Armazena id do filme
  private movieId: string;

  // Armazena o objeto de filme com resultado da API
  public movie: Movie;

  // Flag que indica carregamento da requisição
  public loading: boolean;

  // Flag que indica se a requisição teve resultado ou não
  public noResult: boolean;

  // Armazena subscription do obsevable
  public subscription: Subscription;

  constructor(
    private location: Location,
    private activatedRoute: ActivatedRoute,
    private movieService: MovieService
  ) { }

  /**
  * Méto executado na criação do componente
  */
  ngOnInit(): void {
    this.movieId = this.activatedRoute.snapshot.paramMap.get('id');

    this.loading = true;

    this.subscription = this.movieService.getById(
      {
        plot: 'full',
        i: this.movieId
      }
    ).subscribe(
      result => {
        this.loading = false;
        this.noResult = result.Response.toLowerCase() === 'false';

        if (this.noResult) return;

        this.movie = result
      });
  }

  /**
  * Méto executado quando o componente é destruido
  */
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  /**
   * Método para voltar à rota anterior
   *
   * @memberof DetailsComponent
   */
  back() {
    this.location.back();
  }

}
