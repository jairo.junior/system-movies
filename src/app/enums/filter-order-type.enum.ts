export enum FilterOrderType {
    //Ordenação A - Z
    alphabeticalAToZ = 1,

    //Ordenação Z - A
    alphabeticalZToA = 2,

    //Ordenação Média Maior
    higherAverage = 3,

    //Ordenação Média Menor
    lowerAverage = 4,
}