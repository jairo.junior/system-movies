import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ThemeIdentifier } from 'src/app/identifiers/theme.identifier';

@Component({
  selector: 'header-system',
  templateUrl: './header-system.component.html',
  styleUrls: ['./header-system.component.scss']
})
export class HeaderSystemComponent {

  // Recebe url do logo do cabeçalho
  @Input() logo: string;

  // Recebe título do cabeçalho
  @Input() text: string;

  // Recebe o tema selecionado para marcar o atual no meu
  @Input() theme: ThemeIdentifier;

  // Evento emitido selecionar alguma opção de tema
  @Output() themeChange = new EventEmitter<ThemeIdentifier>();

  // Armazena identificadores de tema (light/dark)
  public themeIdentifier = ThemeIdentifier;
}
