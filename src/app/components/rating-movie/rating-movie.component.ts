import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'rating-movie',
  templateUrl: './rating-movie.component.html',
  styleUrls: ['./rating-movie.component.scss']
})
export class RatingMovieComponent implements OnInit {

  // Recebe quanditade máxima de estrelas a serem exibidas
  @Input() limit: number = 5;

  // Recebe porcentagem a ser preenchida das estrelas
  @Input() value: number;

  // Armazena status de cada estrela baseado na porcentagem
  public arrayStars: string[] = [];

  /**
  * Méto executado na criação do componente
  */
  ngOnInit(): void {
    if (this.limit && this.value) {
      this.buildStars();
    }
  }


  /**
   * Método que popula array com status das estrelas
   *
   * @memberof RatingMovieComponent
   */
  buildStars() {
    let calc = (this.value * this.limit) / 100;
    let half = false;

    for (let i = 1; i <= this.limit; i++) {
      this.arrayStars[i - 1] = 'empty';

      if (i <= Math.floor(calc)) {
        this.arrayStars[i - 1] = 'complete';
      }

      if ((i > calc) && (calc - (Math.floor(calc)) >= 0.5)) {
        if (!half) {
          this.arrayStars[i - 1] = 'half';
          half = true;
        }
      }
    }
  }

}
