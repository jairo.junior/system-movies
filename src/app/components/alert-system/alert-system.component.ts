import { Component, HostBinding, Input } from '@angular/core';

@Component({
  selector: 'alert-system',
  templateUrl: './alert-system.component.html',
  styleUrls: ['./alert-system.component.scss']
})
export class AlertSystemComponent {

  // Aplica classe no host de acordo com o tipo selecionado
  @HostBinding('class')

  // Recebe o tipo de alerta que deve ser exibido
  @Input() type: 'warn' | 'info' | 'success' | 'error' = 'warn';

  // Recebe mensagem a ser exibida no alerta
  @Input() message: string;
}
