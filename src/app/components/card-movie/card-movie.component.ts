import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'card-movie',
  templateUrl: './card-movie.component.html',
  styleUrls: ['./card-movie.component.scss']
})
export class CardMovieComponent  {
  // Recebe nome do filme
  @Input() name: string;

  // Recebe imagem de poste do filme
  @Input() poster: string;

  // Recebe média de ratins já calculada
  @Input() rating: number;

  // Evento emitido ao clicar no botão de detalhes
  @Output() openDetails = new EventEmitter();

  // Armazena o limite de estrelas que aparecerá no rating
  public limitRating: number = 5;
}
