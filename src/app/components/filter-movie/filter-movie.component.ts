import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FilterOrderType } from 'src/app/enums/filter-order-type.enum';

@Component({
  selector: 'filter-movie',
  templateUrl: './filter-movie.component.html',
  styleUrls: ['./filter-movie.component.scss']
})
export class FilterMovieComponent implements OnInit {

  // Evento emitido ao clicar no botão filtrar enviando os dados filtro para a tela
  @Output() filter = new EventEmitter<unknown>();

  // Evento emitido ao clicar no botão limpar
  @Output() clear = new EventEmitter();

  // Usado para criar os controles do formuládio
  public formFilter: FormGroup;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder
  ) { }

  /**
   * Méto executado na criação do componente
   */
  ngOnInit(): void {
    this.formFilter = this.formBuilder.group(
      {
        name: ['', [Validators.required]],
        year: ['']
      });

    this.getQuery();
  }

  /**
   * Método executado ao submeter formulário de filtro
   *
   * @memberof FilterMovieComponent
   */
  onSubmitFilter() {
    this.filter.emit(this.formFilter.value);
    this.setQuery(this.formFilter.value);
  }

  /**
   * Método que reseta formulário de filtro
   *
   * @param {NgForm} form
   * @memberof FilterMovieComponent
   */
  resetForm(form:NgForm) {
    form.resetForm();
    this.clear.emit();
  }

  /**
   * Método que salva dados do filtro na questring para ser recuperado caso haja reload na página
   *
   * @param {unknown} params
   * @memberof FilterMovieComponent
   */
  setQuery(params: unknown) {
    this.router.navigate(['.'], { relativeTo: this.activatedRoute, queryParams: params });
  }

  /**
   * Método que recupera dados da questring para o filtro
   *
   * @memberof FilterMovieComponent
   */
  getQuery() {
    this.activatedRoute.queryParams.subscribe(params => {
      let controls = this.formFilter.controls;

      for (const key in controls) {
        if (Object.prototype.hasOwnProperty.call(controls, key)) {
          controls[key].setValue(params[key]);
        }
      }

      this.filter.emit(this.formFilter.value);
    })
  }

  /**
   * Método para validar se o formulário está em sem preenchimento
   *
   * @readonly
   * @type {boolean}
   * @memberof FilterMovieComponent
   */
  get formsIsEmpty(): boolean {
    for (const key in this.formFilter.controls) {
      let field = this.formFilter.controls[key];

      return field.value == null || field.value == '';
    }
  }
}
