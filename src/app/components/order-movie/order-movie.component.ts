import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatSelectChange } from '@angular/material/select';
import { FilterOrderType } from 'src/app/enums/filter-order-type.enum';

@Component({
  selector: 'order-movie',
  templateUrl: './order-movie.component.html',
  styleUrls: ['./order-movie.component.scss']
})
export class OrderMovieComponent {

  // Recebe objeto com opções de ordenação
  @Input() orders: unknown;

  // Recebe ordenação selecionada
  @Input() selected: FilterOrderType;

  // Evento emitido ao selecionar alguma ordenação
  @Output() orderChange = new EventEmitter<MatSelectChange>();

}
