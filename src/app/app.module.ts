import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ImgFallbackModule } from 'ngx-img-fallback';

// Imports Angular Material
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
// Fim imports Angular material

import { HeaderSystemComponent } from './components/header-system/header-system.component';
import { HomeComponent } from './pages/home/home.component';
import { DetailsComponent } from './pages/details/details.component';
import { CardMovieComponent } from './components/card-movie/card-movie.component';
import { FilterMovieComponent } from './components/filter-movie/filter-movie.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { OrderMovieComponent } from './components/order-movie/order-movie.component';
import { RatingMovieComponent } from './components/rating-movie/rating-movie.component';
import { AlertSystemComponent } from './components/alert-system/alert-system.component';

// Angular Material Components
const MaterialModules = [
  MatButtonModule,
  MatCardModule,
  MatDividerModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatProgressSpinnerModule,
  MatSelectModule,
  MatFormFieldModule
];


@NgModule({
  declarations: [
    AppComponent,
    HeaderSystemComponent,
    HomeComponent,
    DetailsComponent,
    CardMovieComponent,
    FilterMovieComponent,
    OrderMovieComponent,
    RatingMovieComponent,
    AlertSystemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModules,
    ReactiveFormsModule,
    HttpClientModule,
    ImgFallbackModule 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
