export class ThemeIdentifier {

    // Identificador de tema Light
    public static readonly light: string =  "light-theme";

    // Indentificador de tema Dark
    public static readonly dark:string = "dark-theme";
}